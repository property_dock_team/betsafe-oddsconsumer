package com.betsafe.dto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import com.betsafe.DatabaseConnect.JavaConnect;
import com.betsafe.DateConverter.LocalDatePersistenceConverter;


public class SaveToDb {
	
    Connection connect;
    PreparedStatement pstmt;
    ResultSet result;
    LocalDatePersistenceConverter converter = new LocalDatePersistenceConverter();
	public int  getForeignKey(String table,String Key,String KeyValue) {
		ResultSet result= null;
		int foreignKey=0;
		String sql = "SELECT * from "+table.toLowerCase()+" where "+Key+" like '"+KeyValue+"'";
		
		connect = JavaConnect.connectDb();
		PreparedStatement statement2;
		try {
			statement2 = connect.prepareStatement(sql);
			 result = statement2.executeQuery();
				while (result.next()) {
					foreignKey = result.getInt(1);

				}
			
				connect.close();
		}catch(Exception e) {
			
		}
		return foreignKey;
		
	}
	
	
    public void InsertSports(String Sport, String SportID) {
		connect = JavaConnect.connectDb();
		
		try {
			pstmt = connect.prepareStatement(
					"INSERT IGNORE INTO sport (`sport_name`,`created_by`, `priority`, `betradar_sport_id`,`created`) values (?, ?, ?, ?, ?)");
			pstmt.setString(1, Sport);
			pstmt.setString(2, "IsolutionsAPI");
			pstmt.setInt(3, 1);
			pstmt.setInt(4, Integer.parseInt(SportID));
			pstmt.setDate(5, java.sql.Date.valueOf(LocalDate.now()));
		
			pstmt.executeUpdate();
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connect.close();
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    public void InsertGroups(String Group, String GroupID, String Status, String SportID) {
    	
    	
    	int foreignKey=getForeignKey("Sport", "betradar_sport_id", SportID);
		connect = JavaConnect.connectDb();
		
		try {
			pstmt = connect.prepareStatement(
					"INSERT IGNORE INTO category (`category_name`,`status`,`sport_id`,`created_by`,`betradar_category_id`,`created`, `priority`) values (?, ?, ?, ?, ?, ?, ?)");
			pstmt.setString(1, Group);
			pstmt.setInt(2, Integer.parseInt(Status));
			pstmt.setInt(3, foreignKey);
			pstmt.setString(4, "IsolutionsAPI");
			pstmt.setInt(5, Integer.parseInt(GroupID));
			pstmt.setDate(6, java.sql.Date.valueOf(LocalDate.now()));
			pstmt.setInt(7, 1);
			pstmt.executeUpdate();
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connect.close();
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    public void InsertEvents(String Event,String Group, String EventID, String Status,String GroupID, String SportID) {
    	
    	
    	int foreignSport=getForeignKey("Sport", "betradar_sport_id", SportID);
    	int foreignGroup=getForeignKey("category", "betradar_category_id", GroupID);
		connect = JavaConnect.connectDb();
		
		try {
			pstmt = connect.prepareStatement(
					"INSERT IGNORE INTO competition (`competition_name`,`category`,`status`,`category_id`,`sport_id`,`created_by`,`betradar_competition_id`,`created`, `priority`,`ussd_priority`) values (?, ?, ?, ?, ?, ?, ?,?,?,?)");
			pstmt.setString(1, Event);
			pstmt.setString(2, Group);
			pstmt.setInt(3, Integer.parseInt(Status));
			pstmt.setInt(4, foreignSport);
			pstmt.setInt(5, foreignGroup);
			pstmt.setString(6, "IsolutionsAPI");
			pstmt.setInt(7, Integer.parseInt(EventID));
			pstmt.setDate(8, java.sql.Date.valueOf(LocalDate.now()));
			pstmt.setInt(9, 1);
			pstmt.setInt(10, 1);
			pstmt.executeUpdate();
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connect.close();
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    public void InsertSubEvents(String SubEventID,String homeandwayteam, String StartDate, String Status,String EventID) {
    	
    	
    	int foreignEvent=getForeignKey("competition", "betradar_competition_id", EventID);
		connect = JavaConnect.connectDb();
		
		try {
			pstmt = connect.prepareStatement(
					"INSERT INTO `match` (`parent_match_id`,`home_team`,`away_team`,`start_time`,`game_id`,`competition_id`,`status`,`instance_id`,`bet_closure`,`created_by`,`created`,`completed`, `priority`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `start_time` = values( start_time), `bet_closure` = values(bet_closure)");
			pstmt.setInt(1, Integer.parseInt(SubEventID));
			pstmt.setString(2, homeandwayteam.substring(0, homeandwayteam.indexOf("-")-1));
			pstmt.setString(3, homeandwayteam.substring( homeandwayteam.indexOf("-")+1));
			pstmt.setTimestamp(4, converter.stringToEntityAttribute(StartDate.replace("T", " ")));
			pstmt.setInt(5, 1);
			pstmt.setInt(6, foreignEvent);
			pstmt.setInt(7, Integer.parseInt(Status));
			pstmt.setInt(8, 1);
			pstmt.setTimestamp(9, converter.stringToEntityAttribute(StartDate.replace("T", " ")));
			pstmt.setString(10, "IsolutionsAPI");
			pstmt.setDate(11, java.sql.Date.valueOf(LocalDate.now()));
			pstmt.setInt(12, 0);
			pstmt.setInt(13, 50);
			pstmt.executeUpdate();
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connect.close();
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
public void InsertOddType(String OddsClass, String OddsTypeID, String OddsClassCode, String SubEventID,String HND) {
    	
    	
    	//ResultSet resultset=getForeignKey("SportID", SportID, "Sport");
		connect = JavaConnect.connectDb();
		
		try {
			pstmt = connect.prepareStatement(
					"INSERT IGNORE INTO odd_type (`name`,`created_by`,`created`,`active`,`sub_type_id`,`parent_match_id`,`live_bet`,`short_name` ,`priority`,`special_bet_value`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			pstmt.setString(1, OddsClass);
			pstmt.setString(2, "IsolutionsAPI");
			pstmt.setDate(3, java.sql.Date.valueOf(LocalDate.now()));
			pstmt.setInt(4, 1);
			pstmt.setInt(5, Integer.parseInt(OddsTypeID));
			pstmt.setInt(6, Integer.parseInt(SubEventID));
			pstmt.setInt(7, 0);
			pstmt.setString(8, OddsClassCode);
			pstmt.setDouble(9, 0);
			pstmt.setString(10, HND);
			pstmt.executeUpdate();
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connect.close();
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

public void InsertOdds(String OddsClassCode,String Odds, String HND, String Status,String SubEventID, String OddsTypeID,String betradarOddsID) {
	
	
	//ResultSet resultset=getForeignKey("match", "", "Sport");
	connect = JavaConnect.connectDb();
	
	try {
		pstmt = connect.prepareStatement(
				"INSERT INTO event_odd (`parent_match_id`,`sub_type_id`,`max_bet`,`created`,`odd_key`, `odd_value`,`special_bet_value`,`betradar_odd_id`) values (?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `odd_value` = values( odd_value)");
		pstmt.setInt(1, Integer.parseInt(SubEventID));
		pstmt.setInt(2, Integer.parseInt(OddsTypeID));
		pstmt.setDouble(3, 0.00);
		pstmt.setDate(4, java.sql.Date.valueOf(LocalDate.now()));
		pstmt.setString(5, OddsClassCode);
		pstmt.setString(6, Odds);
		pstmt.setString(7, HND);
		pstmt.setString(8, betradarOddsID);

		pstmt.executeUpdate();
	} catch (SQLException e) {
//TODO Auto-generated catch block
		e.printStackTrace();
	}
	try {
		connect.close();
	} catch (SQLException e) {
//TODO Auto-generated catch block
		e.printStackTrace();
	}
}
    
}
