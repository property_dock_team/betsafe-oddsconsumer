package com.betsafe.DateConverter;


import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LocalDatePersistenceConverter implements AttributeConverter<LocalDate, java.sql.Date> {

  @Override
  public java.sql.Date convertToDatabaseColumn(LocalDate entityValue) {
    if (entityValue != null) {
      return java.sql.Date.valueOf(entityValue);
    }
    return null;
  }

  @Override
  public LocalDate convertToEntityAttribute(java.sql.Date databaseValue) {
    if (databaseValue != null) {
      return databaseValue.toLocalDate();
    }
    return null;
  }
  
  public Timestamp stringToEntityAttribute(String databaseValue) {
	    if (databaseValue != null) {
	    	  Timestamp timestamp= null;
	    	try {
	    	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	    java.util.Date parsedDate = dateFormat.parse(databaseValue);
	    	    timestamp = new java.sql.Timestamp(parsedDate.getTime());
	    	} catch(Exception e) { //this generic but you can control another types of exception
	    	    // look the origin of excption 
	    	}
	      return timestamp;
	    }
	    return null;
	  }
  
  
  
  
}
