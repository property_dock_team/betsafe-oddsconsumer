package com.betsafe.consumer;

import org.json.simple.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.betsafe.consumer.parseOdds.ParseOdds;
import com.betsafe.consumer.parseOdds.ReadJsonFile;

@SpringBootApplication
public class OddsConsumerApplication {

	public static void main(String[] args) {
		
		
		SpringApplication.run(OddsConsumerApplication.class, args);
		/*
		 * try { parseOdds.httpGet(); } catch (Exception e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); }
		 */
		ReadJsonFile file = new ReadJsonFile();
		try {
			  JSONObject json=file.readFile();
			
			//JSONObject json=ParseOdds.httpGet();
			ParseOdds odds = new ParseOdds();
			odds.parseJsonObject(json);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
