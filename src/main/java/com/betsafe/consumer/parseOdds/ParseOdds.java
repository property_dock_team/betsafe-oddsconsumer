package com.betsafe.consumer.parseOdds;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.betsafe.dto.SaveToDb;

public class ParseOdds {
	
	SaveToDb save = new SaveToDb();

	public static JSONObject httpGet() throws Exception {
		JSONObject data_obj = null;
		URL url = new URL(" https://apirest.qa.betsafe.co.ke/api/eventprogram/GetEventsProgramV6");
		URLConnection con = url.openConnection();
		HttpURLConnection conn = (HttpURLConnection) con;
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);

		byte[] out = "{\"APIAccount\": \"StartupSystems\",\"APIPassword\":\"xL9%Y4Ee11?kfG5Q\",\"EventsProgramCode\": \"Pal01Betsafe\",\"LanguageID\": 2,\"DateLastModify\": \"1900-01-01T12:30:00\",\"IDBookmaker\": 172}"
				.getBytes(StandardCharsets.UTF_8);
		int length = out.length;

		conn.setFixedLengthStreamingMode(length);
		conn.setRequestProperty("Content-Type", "application/json");

		try (OutputStream os = conn.getOutputStream()) {
			os.write(out);
		}

		conn.connect();
		int responsecode = conn.getResponseCode();
		System.out.println("Response code is" + responsecode);
		if (responsecode != 200) {
			throw new RuntimeException("HttpResponseCode: " + responsecode);
		} else {
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());

			data_obj = stringToJsonObject(response.toString());

		}

		return data_obj;

	}

	public static JSONObject stringToJsonObject(String obj) throws ParseException {

		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(obj);

		return json;

	}

	public  void parseJsonObject(JSONObject obj) {

		// String TransactionType = (String) obj.get("TransactionType");
		// String TransID = (String) obj.get("TransID");
		
		JSONArray Sports = (JSONArray) obj.get("Sports");
	
		JSONObject Groups1 = (JSONObject) Sports.get(0);
		JSONArray Groups = (JSONArray) Groups1.get("Groups");
		
		  JSONObject Events1 = (JSONObject) Groups.get(0);
		  
		  JSONArray Events = (JSONArray) Events1.get("Events");
		  JSONObject SubEvents1 = (JSONObject) Events.get(0);
		  JSONArray SubEvents = (JSONArray) SubEvents1.get("SubEvents");
		  JSONObject Odds1 = (JSONObject)SubEvents.get(0);
		  JSONArray Odds = (JSONArray)Odds1.get("Odds");
		 
	
		  
		 getSports(Sports);
		//System.out.println( Odds.get(0));

		// System.out.println("Sports :: "+Sports.get(0)+"Groups
		// ::"+Groups.get(0)+"Events ::"+ Events.get(0)+"SubEvents
		// :"+SubEvents.get(0)+"Odds : "+Odds.get(0));

	}
	
	@SuppressWarnings("unused")
	public void getSports(JSONArray Sports ) {
		JSONObject Sportsobj;
		

	        
	        String SportID ="";
	        String  Sport ="";
	        String Status = "";
	        String Order ="";
	        
	        int counter =-1;
           
	       for ( int i= 0;  i < Sports.size(); i++) {
	    	   counter++;
	        	Sportsobj=(JSONObject)Sports.get(i);
	        	SportID=Sportsobj.get("SportID").toString();
	        	Sport=Sportsobj.get("Sport").toString();
	        	Status=Sportsobj.get("Status").toString();
	        	Order=Sportsobj.get("Order").toString();
				JSONArray Groups = (JSONArray) Sportsobj.get("Groups");
	        	save.InsertSports(Sport, SportID);
	        	getGroups(Groups,SportID);
	        }
	        
	        System.out.println("Sport ID "+ SportID+" Sport "+Sport+" Status "+Status+" Order "+Order);
	        
		
		
	}
	@SuppressWarnings("unused")
	public void getGroups(JSONArray Groups, String SportID ) {
		JSONObject Groupsobj;
	      
			
	        
	        String GroupID ="";
	        String  Group ="";
	        String Status = "";
	        String Order ="";
	        int counter=-1;
	           
	       for ( int i= 0;  i < Groups.size(); i++) {
	    	   counter++;
	        	

	        	Groupsobj=(JSONObject) Groups.get(i);
	        	GroupID=Groupsobj.get("GroupID").toString();
	        	Group=Groupsobj.get("Group").toString();
	        	Status=Groupsobj.get("Status").toString();
	        	Order=Groupsobj.get("Order").toString();
	        	JSONArray Events = (JSONArray) Groupsobj.get("Events");
	        	save.InsertGroups(Group, GroupID, Status, SportID);
	        	getEvents(Events, Group,GroupID,SportID );
	      
	       }
	        System.out.println("Group ID "+ GroupID+" Group "+Group+" Status "+Status+" Order "+Order);
	        
		
		
	}
	
	
	@SuppressWarnings("unused")
	public void getEvents(JSONArray Events,String Group,String GroupID, String SportID  ) {
		JSONObject Eventsobj;
	        @SuppressWarnings("rawtypes")
			Iterator objIter = Events.iterator();
	        
	        String EventID ="";
	        String  Event ="";
	        String Status = "";
	        String Order ="";
	        int counter = -1;
	        for( int i=0; i< Events.size(); i++) {
	        	counter++;
	        	Eventsobj=(JSONObject) Events.get(i);
	        	EventID=Eventsobj.get("EventID").toString();
	        	Event=Eventsobj.get("Event").toString();
	        	Status=Eventsobj.get("Status").toString();
	        	Order=Eventsobj.get("Order").toString();
	        	JSONArray SubEvents = (JSONArray) Eventsobj.get("SubEvents");
	        	save.InsertEvents(Event,Group,EventID, Status,GroupID, SportID);
	        	getSubEvents(SubEvents,EventID );
	        }
	        
	        System.out.println("Event ID "+ EventID+" Event "+Event+" Status "+Status+" Order "+Order);
	        
		
		
	}
	
	@SuppressWarnings("unused")
	public void getSubEvents(JSONArray SubEvents,String EventID) {
		JSONObject SubEventsobj;
		
//	        @SuppressWarnings("rawtypes")
//			Iterator objIter = SubEvents.iterator();
	        
	        String SubEventID ="";
	        String  SubEvent ="";
	        String  StartDate = "";
	        String Status = "";
	        String Order ="";
	        
	        int counter=-1;
	        for(int i=0; i< SubEvents.size(); i++) {
	        	counter ++;
	        	SubEventsobj=(JSONObject) SubEvents.get(i);
	        	SubEventID=SubEventsobj.get("SubEventID").toString();
	        	SubEvent=SubEventsobj.get("SubEvent").toString();
	        	StartDate=SubEventsobj.get("StartDate").toString();
	        	Status=SubEventsobj.get("Status").toString();
	        	Order=SubEventsobj.get("Order").toString();
	        	JSONArray Odds = (JSONArray) SubEventsobj.get("Odds");
	        	save.InsertSubEvents(SubEventID,SubEvent, StartDate, Status,EventID);
	        	 getOdds(Odds, SubEventID);
	        	 System.out.println("SubEvent ID "+ SubEventID+" SubEvent "+SubEvent+" Status "+Status+" Order "+Order);
	        }
	        
	        System.out.println("SubEvent ID "+ SubEventID+" SubEvent "+SubEvent+" Status "+Status+" Order "+Order);
	        
		
		
	}
	@SuppressWarnings("unused")
	public void getOdds(JSONArray Odds, String SubEventID ) {
		JSONObject Oddsobj;

	        
	        String OddsTypeID ="";
	        String OddsID ="";
	        String OddValue ="";
	        String HND = "";
	        String  OddsClass ="";
	        String  StartDate = "";
	        String Status = "";
	        String Order ="";
	        String OddsClassCode="";
	        String betradarOddsID="";
	        for (int i=0; i< Odds.size(); i++) {
	        	Oddsobj=(JSONObject) Odds.get(i);
	        	betradarOddsID=Oddsobj.get("OddsID").toString();
	        	OddsTypeID=Oddsobj.get("OddsTypeID").toString();
	        	OddsID=Oddsobj.get("OddsTypeID").toString();
	        	OddsClass=Oddsobj.get("OddsClass").toString();
	        	OddsClassCode=Oddsobj.get("OddsClassCode").toString();
	        	OddValue=Oddsobj.get("Odds").toString();
	        	HND=Oddsobj.get("HND").toString();
	        	StartDate=Oddsobj.get("StartDate").toString();
	        	Status=Oddsobj.get("Status").toString();
	        	//Order=Oddsobj.get("Order").toString();
	        	save.InsertOddType(OddsClass, OddsTypeID, OddsClassCode, SubEventID,HND);
	        	save.InsertOdds(OddsClassCode,OddValue, HND,Status, SubEventID, OddsTypeID,betradarOddsID);
	       	        }
	       
		
		
	}

}
