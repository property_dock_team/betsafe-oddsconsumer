package com.betsafe.consumer.parseOdds;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
public class ReadJsonFile {
	
	
	
	public JSONObject readFile() throws Exception, IOException {
		
		String filepath = new File(".").getCanonicalPath() + "/src/main/resources/oddsJson/databts.json";
	    JSONParser jsonParser = new JSONParser();
	    JSONObject json= new  JSONObject();
	    try (FileReader reader = new FileReader(filepath)){
	    	  json =(JSONObject) jsonParser.parse(reader);
	    	 //System.out.println(json.toString());
	    	
	    }catch(Exception e) {
	    	System.out.println(e.toString());
	    }
		return json;
	}

}
